using System;
using System.Net;
using System.Net.Sockets;

namespace Shared {
    public class Client : BaseConnection {

        public override void Start(IPAddress address, int port) {
            if (InitializeSocket()) throw new Exception("HELP!");

            socket.BeginConnect(address, port, ar => {
                var conn = (Socket)ar.AsyncState;
                conn.EndConnect(ar);
                Read(conn);
                OnConnected(null);
            }, socket);
        }

        public override void Send(Message message) {
            Send(socket, message);
        }

        public void Send(Package package) {
            Send(socket, package);
        }

        public override void Close() {
            isClosing = true;
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }
    }
}