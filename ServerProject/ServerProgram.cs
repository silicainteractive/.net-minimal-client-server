﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using Shared;

namespace ServerProject {

    class ServerProgram {
        private static Server server;
        static void Main(string[] args) {
            if (args == null || args.Length < 2) {
                args = new string[2];
                Console.Write("Please specify IP address: ");
                args[0] = Console.ReadLine();
                Console.Write("Please specify port number: ");
                args[1] = Console.ReadLine();
            }

            server = new Server();
            SocketInfo info;

            try {
                info = new SocketInfo(args[0], args[1]);
            } catch (ArgumentException e) {
                Console.WriteLine(e.Message);
                return;
            }

            server.name = args.Length > 2 ? args[2] : "Server";
            var waitForConnection = false;
            server.Connected += sender => {
                Console.WriteLine("{0} connected", server.name);
                waitForConnection = true;
                server.Send(sender, new Message { command = "WHORU" });
            };

            server.Received += HandleMessage;

            server.Start(info);
            while (!waitForConnection) {
            }
            while (server.IsConnected) {
                var readLine = Console.ReadLine();
                if (readLine == null) continue;

                var line = readLine.Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
                var command = line[0].ToUpper();
                if (command == "EXIT") {
                    server.Close();
                    break;
                }

                switch (command) {
                    case "SEND":
                        if (line.Count() <= 1) continue;
                        var m = new Message(line[1]);
                        server.Send(m);
                        break;
                    case "WHO":
                        Console.WriteLine(server.GetConnections());
                        break;
                }
            }
        }

        private static void HandleMessage(Socket s, Message m) {
            if (m.command == null) return;
            switch (m.command.ToUpper()) {
                case "PASSTHROUGH":
                    if (!server.Send((string)m.args[0], m))
                        server.Send(s, new Message("MESSAGE FROM SERVER: Could not find client {0}", m.args[0]));
                    break;
                case "BROADCAST":
                    foreach (var c in server.connections.Values.Where(c => s != c)) {
                        server.Send(c, m);
                    }
                    break;
                case "WHO":
                    server.Send(s, new Message(server.GetConnections()));
                    break;
                case "IDENTIFY":
                    var name = (string)m.args[0];
                    server.AddConnection(ref name, s);
                    Console.WriteLine(name + " identified!");
                    break;
                default:
                    Console.WriteLine(m);
                    break;
            }
        }
    }
}
