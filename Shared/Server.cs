using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Shared {
    public class Server : BaseConnection {
        public Socket Connection { get; private set; }
        public readonly Dictionary<string, Socket> connections;

        private const int MaxConnections = 100;
        private int connectionCount;


        public Server() {
            connections = new Dictionary<string, Socket>();
        }

        public bool AddConnection(ref string cn, Socket s) {
            var i = 1;

            while (connections.ContainsKey(cn)) {
                if (connections[cn] == s) return false;
                cn += i++;
            }
            connections.Add(cn, s);
            return true;
        }
        public override bool IsConnected {
            get { return connectionCount > 0; }
        }

        public override void Start(IPAddress address, int port) {
            if (InitializeSocket()) throw new Exception("HELP!");

            var ipe = new IPEndPoint(address, port);
            socket.Bind(ipe);
            socket.Listen(100);
            socket.BeginAccept(OnAccept, socket);
        }

        private void OnAccept(IAsyncResult ar) {
            if (isClosing) return;
            var conn = (Socket)ar.AsyncState;
            var c = conn.EndAccept(ar);
            connectionCount++;
            Read(c);
            OnConnected(c);
            if (connectionCount < MaxConnections)
                socket.BeginAccept(OnAccept, socket);
        }


        public override void Send(Message message) {
            connections.Values.ToList().ForEach(s => Send(s, message));
        }

        public bool Send(string clientName, Message m) {
            if (!connections.ContainsKey(clientName)) return false;
            Send(connections[clientName], m);
            return true;
        }

        public override void Close() {
            isClosing = true;
            socket.Close();
            foreach (var kv in connections) {
                kv.Value.Shutdown(SocketShutdown.Both);
                kv.Value.Close();
            }
        }

        public string GetConnections() {
            var sb = new StringBuilder();
            foreach (var kv in connections) {
                sb.AppendLine(string.Format("{0} \t {1}", kv.Key, kv.Value.GetHashCode()));
            }
            return sb.ToString();
        }
    }
}