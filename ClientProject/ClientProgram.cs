﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Shared;

namespace ClientProject {
    class ClientProgram {
        static void Main(string[] args) {
            if (args == null || args.Length < 2) {
                args = new string[3];
                Console.Write("Please specify IP address: ");
                args[0] = Console.ReadLine();
                Console.Write("Please specify port number: ");
                args[1] = Console.ReadLine();
                Console.Write("Please specify name: ");
                args[2] = Console.ReadLine();
                if (args[2] == null || args[2].Length <= 0) args[2] = "Client";
            }

            var client = new Client();
            SocketInfo info;

            try {
                info = new SocketInfo(args[0], args[1]);
            } catch (ArgumentException e) {
                Console.WriteLine(e.Message);
                return;
            }

            client.name = args.Length > 2 ? args[2] : "Client";
            var waitForConnection = false;
            client.Connected += sender => {
                Console.WriteLine("{0} connected", client.name);
                waitForConnection = true;
            };

            client.Received += (s, m) => {
                switch (m.command) {
                    case "WHORU":
                        Console.WriteLine("Identifying");
                        client.Send(new Message { command = "IDENTIFY", args = new[] { (object)client.name } });
                        break;
                    default:
                        Console.WriteLine(m);
                        break;
                }
            };

            client.Start(info);

            while (!waitForConnection) {
            }
            while (client.IsConnected) {
                var readLine = Console.ReadLine();
                if (readLine == null) continue;

                var line = readLine.Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
                var command = line[0].ToUpper();
                if (command == "EXIT") {
                    client.Close();
                    continue;
                }
                switch (command) {
                    case "SEND":
                        if (line.Count() <= 1) continue;
                        var a = line[1].Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
                        client.Send(a.Count() < 2
                            ? new Message(client.name + ": " + a[0])
                            : new Message(client.name + ": " + a[1]) { command = "PASSTHROUGH", args = new[] { (object)a[0] } });
                        break;
                    case "BROADCAST":
                        client.Send(new Message(client.name + ": " + line[1]) { command = "BROADCAST" });
                        break;
                    case "WHO":
                        client.Send(new Message(" ") { command = "WHO" });
                        break;
                }
            }
        }
    }
}
