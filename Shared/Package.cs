﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Shared {
    [Serializable]
    public class Package : ISerializable {
        private readonly Guid id;
        public string type;
        public object[] data;

        public Package() {
            id = Guid.NewGuid();
        }

        public Package(SerializationInfo info, StreamingContext context) {
            id = (Guid)info.GetValue("id", typeof(Guid));
            type = (string)info.GetValue("type", typeof(string));
            data = (object[])info.GetValue("data", typeof(object[]));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context) {
            info.AddValue("id", id, typeof(Guid));
            info.AddValue("type", type, typeof(string));
            info.AddValue("data", data, typeof(object[]));
        }

        public byte[] Serialize() {
            byte[] retval;
            using (var ms = new MemoryStream()) {
                var f = new BinaryFormatter();
                f.Serialize(ms, this);
                retval = ms.ToArray();
            }

            return retval;
        }

        public static Message Deserialize(byte[] data) {
            Message retval;
            using (var ms = new MemoryStream(data)) {
                var f = new BinaryFormatter();
                retval = (Message)f.Deserialize(ms);
            }
            return retval;
        }

        public override string ToString() {
            return string.Format(type);
        }

        public override bool Equals(object obj) {
            var message = obj as Package;
            if (message == null) return false;
            return message.id == id;
        }

        public override int GetHashCode() {
            return id.GetHashCode();
        }
    }
}
