﻿using System;
using System.Net;

namespace Shared {
    public class SocketInfo {
        public readonly IPAddress address;
        public readonly int port;
        public SocketInfo(IPAddress address, int port) {
            this.address = address;
            this.port = port;
        }

        public SocketInfo(string address, string port) {

            var a = address.ToLower() == "localhost" ? IPAddress.Loopback : IPAddress.Parse(address);

            int p;
            if (!int.TryParse(port, out p)) throw new ArgumentException("Port number could not be verified");

            this.address = a;
            this.port = p;
        }
    }
}
