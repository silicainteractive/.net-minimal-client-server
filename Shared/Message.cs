﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Shared {
    [Serializable]
    public class Message : ISerializable {
        private static readonly ObjectIDGenerator MessageIDGenerator = new ObjectIDGenerator();
        private readonly Guid id;
        public string message = "";
        public string command;
        public object[] args;

        public Message() {
            id = Guid.NewGuid();
        }

        public Message(string message, params object[] args)
            : this() {
            this.message = string.Format(message, args);
        }


        public Message(SerializationInfo info, StreamingContext context) {
            id = (Guid)info.GetValue("id", typeof(Guid));
            message = (string)info.GetValue("message", typeof(string));
            command = (string)info.GetValue("command", typeof(string));
            args = (object[])info.GetValue("args", typeof(object[]));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context) {
            info.AddValue("id", id, typeof(Guid));
            info.AddValue("message", message, typeof(string));
            info.AddValue("command", command, typeof(string));
            info.AddValue("args", args, typeof(object[]));
        }

        public byte[] GetBytes() {
            byte[] retval;
            using (var ms = new MemoryStream()) {
                var f = new BinaryFormatter();
                f.Serialize(ms, this);
                retval = ms.ToArray();
            }

            return retval;
        }

        public static Message Deserialize(byte[] data) {
            Message retval;
            using (var ms = new MemoryStream(data)) {
                var f = new BinaryFormatter();
                retval = (Message)f.Deserialize(ms);
            }
            return retval;
        }

        public override string ToString() {
            return string.Format(message);
        }

        public override bool Equals(object obj) {
            var m = obj as Message;
            if (m == null) return false;
            return m.id == id;
        }

        public override int GetHashCode() {
            return id.GetHashCode();
        }
    }
}
