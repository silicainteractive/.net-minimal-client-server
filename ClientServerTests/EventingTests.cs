﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shared;

namespace ClientServerTests {
    [TestClass]
    public class EventingTests {
        [TestMethod]
        public void EventTest() {
            var client = new Client { name = "Client" };
            var server = new Server { name = "Server" };

            var b = false;
            var m = new Message() { message = "Hello" };
            server.Received += (s, message) => {
                Console.WriteLine("Received message {0}", message);
                Assert.AreEqual(m, message);
                b = true;
            };

            var info = new SocketInfo(IPAddress.Loopback, 1336);

            var ct = new Thread(() => {
                client.Start(info);
                client.Connected += (o) => client.Send(m);
            });

            var st = new Thread(() => server.Start(info));
            st.Start();
            ct.Start();
            while (!b) {
                Thread.Sleep(100);
            }
            client.Close(); server.Close();
            st.Join();
            ct.Join();
        }
    }
}
