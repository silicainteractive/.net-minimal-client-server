﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Timers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shared;

namespace ClientServerTests {
    [TestClass]
    public class ConnectionTests {
        [TestMethod]
        public void ClientSendTest() {
            var client = new Client { name = "Client" };
            var server = new Server { name = "Server" };
            var b = false;
            var info = new SocketInfo(IPAddress.Loopback, 1336);
            var m = new Message("hello");

            var clientThread = new Thread(() => {
                client.Start(info);
                client.Connected += o => client.Send(m);
            });

            var serverThread = new Thread(() => {
                server.Start(info);
                while (server.receivedMessages.Count <= 0) {
                    Thread.Sleep(100);
                }
                b = true;
            });
            serverThread.Start();
            Thread.Sleep(100);
            clientThread.Start();
            while (!b)
                Thread.Sleep(100);
            Assert.AreEqual(m, server.receivedMessages.Last());
            client.Close(); server.Close();
            serverThread.Join();
            clientThread.Join();

        }
        [TestMethod]
        public void ServerSendTest() {
            var client = new Client { name = "Client" };
            var server = new Server { name = "Server" };
            var b = false;
            var info = new SocketInfo(IPAddress.Loopback, 1336);
            var m = new Message("hello");

            var serverThread = new Thread(() => {
                server.Start(info);
                server.Connected += o => server.Send(m);
            });

            var clientThread = new Thread(() => {
                client.Start(info);
                Thread.Sleep(1000);

                while (client.receivedMessages.Count <= 0) {
                    Thread.Sleep(100);
                }
                b = true;
            });
            serverThread.Start();
            Thread.Sleep(5000);
            clientThread.Start();
            while (!b)
                Thread.Sleep(100);
            Assert.AreEqual(m, server.receivedMessages.Last());
            client.Close(); server.Close();
            serverThread.Join();
            clientThread.Join();

        }

        [TestMethod]
        public void StressTest() {
            Console.WriteLine("hello");
            var server = new Server { name = "Server" };
            var clients = new List<Client>();
            var clientThreads = new List<Thread>();
            var info = new SocketInfo(IPAddress.Loopback, 1336);
            var serverThread = new Thread(() => server.Start(info));
            serverThread.Start();
            for (var i = 1; i < 100; i++) {
                var c = new Client { name = "Client" + i };
                clients.Add(c);
                clientThreads.Add(new Thread(() => c.Start(info)));
                c.Received += (socket, message) =>
                              {
                                  Console.WriteLine("message!");
                                  Assert.AreEqual(message, c.receivedMessages.Last());
                              };
            }
            clientThreads.ForEach(t => t.Start());
            var r = new Random().Next(0, clientThreads.Count - 1);
            clients[r].Send(new Message("WHAT UP PPL!") { command = "BROADCAST" });
            clients.ForEach(c => c.Close());
            server.Close();
            serverThread.Join();
            clientThreads.ForEach(t => t.Join());
        }
    }
}
