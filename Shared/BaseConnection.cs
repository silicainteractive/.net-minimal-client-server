﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Shared {

    public delegate void ConnectionHandler(Socket s);
    public abstract class BaseConnection {
        public string name;
        public readonly List<Message> receivedMessages;

        public event ConnectionHandler Connected;
        public event Action<Socket, Message> Received;

        protected Socket socket;
        protected byte[] buffer;
        protected bool isClosing;

        public virtual bool IsConnected {
            get { return socket.Connected; }
        }

        protected virtual void OnConnected(Socket other) {
            var handler = Connected;
            if (handler != null) handler(other);
        }


        protected BaseConnection() {
            receivedMessages = new List<Message>();
        }

        public void Start(SocketInfo info) {
            Start(info.address, info.port);
        }

        public abstract void Start(IPAddress address, int port);
        protected void Read(Socket s) {
            buffer = new byte[s.ReceiveBufferSize];
            s.BeginReceive(buffer, 0, buffer.Length, 0, OnRead, s);
        }
        private void OnRead(IAsyncResult ar) {

            var conn = (Socket)ar.AsyncState;
            if (!conn.Connected) return;
            var read = conn.EndReceive(ar);
            if (read > 0) {

                Trace.WriteLine("----------");
                Trace.WriteLine(string.Format("{0} is reading: ", name));
                var message = Message.Deserialize(buffer);
                receivedMessages.Add(message);
                Trace.WriteLine(string.Format(message.ToString()));
                Trace.WriteLine("----------");
                var r = Received;
                if (r != null)
                    r(conn, message);
            }
            if (isClosing) return;
            conn.BeginReceive(buffer, 0, buffer.Length, 0, OnRead, conn);
        }

        public abstract void Send(Message message);
        public void Send(Socket s, string message) {
            var byteData = Encoding.ASCII.GetBytes(message);

            s.BeginSend(byteData, 0, byteData.Length, 0, ar => {
                var conn = (Socket)ar.AsyncState;
                conn.EndSend(ar);
            }, s);
        }

        public void Send(Socket s, Message m) {
            SendBytes(s, m.GetBytes());
        }

        protected void Send(Socket s, Package p) {
            SendBytes(s, p.Serialize());
        }

        private static void SendBytes(Socket s, byte[] data) {
            s.BeginSend(data, 0, data.Length, 0, ar => {
                var conn = (Socket)ar.AsyncState;
                conn.EndSend(ar);
            }, s);

        }
        protected bool InitializeSocket() {
            if (socket == null)
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            return socket.Connected;
        }

        public abstract void Close();
    }

}
